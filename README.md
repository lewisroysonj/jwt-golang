# jwt-golang

A [golang](http://www.golang.org) implementation of [JSON Web Tokens](http://self-issued.info/docs/draft-ietf-oauth-json-web-token.html) wrapping [jwt-go](https://github.com/dgrijalva/jwt-go)


**TODO LIST:** Enable customization of Claims and Signing Method. The current version supports the following signing algorithms: SigningMethodHS256.

## How to use it

```golang
import "gitlab.com/pragmaticreviews/jwt-golang"
```